package main

import ("net/http"
	"io")


func GetRandomHTML(url string) io.ReadCloser {

	c := http.Client{}
	resp, _ := c.Get(url)
	
	return resp.Body
}
